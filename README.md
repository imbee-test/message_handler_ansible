# message_handler_ansible



## How to use
1. Fill all the missing vaiables
2. Encrypt password
```
ansible-vault encrypt_string "xxx"
```
3. Run ansible playbook
```
ansible-playbook -i production gitlab-runner.yml --diff -ask-vault-pass
ansible-playbook -i production message-handler-server.yml --diff -ask-vault-pass
```